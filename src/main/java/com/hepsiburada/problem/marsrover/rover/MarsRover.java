package com.hepsiburada.problem.marsrover.rover;

import java.awt.Point;
import java.util.Map;

import com.hepsiburada.problem.marsrover.command.ICommand;
import com.hepsiburada.problem.marsrover.command.MoveCommand;
import com.hepsiburada.problem.marsrover.command.TurnCommand;
import com.hepsiburada.problem.marsrover.constant.Direction;
import com.hepsiburada.problem.marsrover.constant.TurnWay;

public class MarsRover implements IMarsRover {

	private Point coordinate;
	private Direction direction;
	final Map<Character, ICommand> commandMap = 
			Map.of(
				'L', new TurnCommand(TurnWay.LEFT), 
				'R', new TurnCommand(TurnWay.RIGHT), 
				'M',new MoveCommand());

	public MarsRover() {
		coordinate = new Point();
	}

	@Override
	public void setCoordinate(int x, int y, Direction direction) {
		coordinate.setLocation(x, y);
		this.direction = direction;
	}

	@Override
	public void printCoordinate() {
		System.out.println("Current coordinate is: " + coordinate.x + "," + coordinate.y + " " + direction.getVal());
	}

	@Override
	public void apply(String command) {
		char[] commandList = command.toCharArray();
		for (char value : commandList) {
			applyCommand(value);
		}
	}

	private void applyCommand(char value) {
		ICommand command = commandMap.get(value);
		if (command == null) {
			throw new IllegalArgumentException("Command is invalid.");
		}
		command.apply(this);
	}

	@Override
	public Point getCoordinate() {
		return coordinate;
	}

	@Override
	public Direction getDirection() {
		return direction;
	}

	@Override
	public void setDirection(Direction direction) {
		this.direction = direction;
	}
}
