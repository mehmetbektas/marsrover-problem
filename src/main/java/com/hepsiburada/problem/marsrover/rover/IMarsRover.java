package com.hepsiburada.problem.marsrover.rover;

import java.awt.Point;

import com.hepsiburada.problem.marsrover.constant.Direction;
import com.hepsiburada.problem.marsrover.constant.TurnWay;

public interface IMarsRover {

	/**
	 * Sets coordinate and direction
	 * 
	 * @param x
	 * @param y
	 * @param direction
	 */
	void setCoordinate(int x, int y, Direction direction);

	/**
	 * Shows current coordinate and direction
	 */
	void printCoordinate();


	/**
	 * Applies given command
	 * 
	 * @param command
	 */
	void apply(String command);
	
	/**
	 * Gives current coordinate
	 * @return Point
	 */
	Point getCoordinate();
	
	/**
	 * Gives current direction
	 * @return Direction
	 */
	Direction getDirection();

	/**
	 * Sets directon 
	 * @param direction
	 */
	void setDirection(Direction direction);
	
	
}
