package com.hepsiburada.problem.marsrover;

import com.hepsiburada.problem.marsrover.constant.Direction;
import com.hepsiburada.problem.marsrover.rover.IMarsRover;
import com.hepsiburada.problem.marsrover.rover.MarsRover;

public class MarsRoverApp {

	public static void main(String[] args) {
		IMarsRover marsRover = new MarsRover();
		marsRover.setCoordinate(1, 2, Direction.NORTH);
		marsRover.apply("LMLMLMLMM");
		marsRover.printCoordinate();
		marsRover.setCoordinate(3, 3, Direction.EAST);
		marsRover.apply("MMRMMRMRRM");
		marsRover.printCoordinate();
	}

}
