package com.hepsiburada.problem.marsrover.constant;

/**
 * Enum type of TurnWay
 * @author mehmetakif
 */
public enum TurnWay {
	LEFT(90),
	RIGHT(-90);
	
	private int val;
	
	private TurnWay(int val) {
		this.val = val;
	}
	
	public int getVal() {
		return val;
	}
}
