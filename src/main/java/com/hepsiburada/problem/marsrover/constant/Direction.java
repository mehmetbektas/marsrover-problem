package com.hepsiburada.problem.marsrover.constant;

import java.util.Arrays;

/**
 * Enum type of Direction
 * 
 * @author mehmetakif
 */
public enum Direction {
	NORTH('N', 90), EAST('E', 0), SOUTH('S', 270), WEST('W', 180);

	private char val;
	private int degree;

	private Direction(char val, int degree) {
		this.val = val;
		this.degree = degree;
	}

	public char getVal() {
		return val;
	}

	public int getDegree() {
		return degree;
	}

	public static Direction getByDegree(int value) {
		int valueMod = (value + 360) % 360; 
		return Arrays.asList(Direction.values()).stream().filter(e -> e.getDegree() == valueMod).findAny().get();
	}

}
