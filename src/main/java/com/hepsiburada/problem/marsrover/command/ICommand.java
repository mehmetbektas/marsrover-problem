package com.hepsiburada.problem.marsrover.command;

import com.hepsiburada.problem.marsrover.rover.IMarsRover;

public interface ICommand {

	void apply(IMarsRover marsRover);
	
}
