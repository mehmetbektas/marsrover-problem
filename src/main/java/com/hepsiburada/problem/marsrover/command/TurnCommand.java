package com.hepsiburada.problem.marsrover.command;

import com.hepsiburada.problem.marsrover.constant.Direction;
import com.hepsiburada.problem.marsrover.constant.TurnWay;
import com.hepsiburada.problem.marsrover.rover.IMarsRover;

public class TurnCommand implements ICommand {

	private final TurnWay turnWay;
	
	public TurnCommand(TurnWay turnWay) {
		this.turnWay = turnWay;
	}
	
	@Override
	public void apply(IMarsRover marsRover) {
		Direction direction = Direction.getByDegree(marsRover.getDirection().getDegree() + turnWay.getVal());
		marsRover.setDirection(direction);
	}

}
