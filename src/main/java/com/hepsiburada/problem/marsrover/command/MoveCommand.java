package com.hepsiburada.problem.marsrover.command;

import java.awt.Point;

import com.hepsiburada.problem.marsrover.rover.IMarsRover;

public class MoveCommand implements ICommand {

	@Override
	public void apply(IMarsRover marsRover) {
		Point coordinate = marsRover.getCoordinate();
		
		switch (marsRover.getDirection()) {
		case NORTH:
			coordinate.move(coordinate.x, coordinate.y + 1);
			break;
		case EAST:
			coordinate.move(coordinate.x + 1, coordinate.y);
			break;
		case SOUTH:
			coordinate.move(coordinate.x, coordinate.y - 1);
			break;
		case WEST:
			coordinate.move(coordinate.x - 1, coordinate.y);
			break;
		}
	}

}
