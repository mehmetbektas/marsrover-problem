package com.hepsiburada.problem.marsover.rover;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.hepsiburada.problem.marsrover.constant.Direction;
import com.hepsiburada.problem.marsrover.constant.TurnWay;
import com.hepsiburada.problem.marsrover.rover.IMarsRover;
import com.hepsiburada.problem.marsrover.rover.MarsRover;

public class MarsRoverTest {

	IMarsRover marsRover;
	
	@BeforeEach
	public void init() {
		marsRover = new MarsRover();
	}
	
	@DisplayName("Set Coordinate Test")
	@Test
	public void setCoordinateTest() {
		marsRover.setCoordinate(3, 2, Direction.EAST);
		assertEquals(marsRover.getCoordinate().x, 3);
		assertEquals(marsRover.getCoordinate().y, 2);
		assertEquals(marsRover.getDirection(), Direction.EAST);

		marsRover.setCoordinate(-5, 8, Direction.NORTH);
		assertEquals(marsRover.getCoordinate().x, -5);
		assertEquals(marsRover.getCoordinate().y, 8);
		assertEquals(marsRover.getDirection(), Direction.NORTH);
	}
	
		
	@DisplayName("Apply Test")
	@Test
	void applyTest() {
		marsRover.setCoordinate(0, 0, Direction.NORTH);
		marsRover.apply("LMMMRMLLMM");
		assertEquals(marsRover.getCoordinate().x, -3);
		assertEquals(marsRover.getCoordinate().y, -1);
		assertEquals(marsRover.getDirection(), Direction.SOUTH);
		
		marsRover.setCoordinate(0, 0, Direction.NORTH);
		assertThrows(IllegalArgumentException.class, ()-> {
			marsRover.apply("RMCLLMM");
		});
		
	}
	
}
